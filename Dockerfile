ARG GO_VERSION=1.18
FROM golang:${GO_VERSION} as builder

RUN mkdir /build

ADD *.go /build/
ADD *.mod /build/
ADD *.sum /build/

WORKDIR /build

RUN CGO_ENABLED=0 GOOS=linux go build -a -o api-sandbox .

FROM alpine as production

ARG TF_VERSION=1.1.7
ARG ARCH=amd64
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_${ARCH}.zip && \
    unzip terraform_${TF_VERSION}_linux_${ARCH}.zip -d /tmp/terraform && \
    rm terraform_${TF_VERSION}_linux_${ARCH}.zip && \
    mv /tmp/terraform/terraform /usr/local/bin/ && \ 
    terraform -help

WORKDIR /app

ENV PORT=8080

COPY --from=builder /build/api-sandbox .

CMD ["./api-sandbox"]