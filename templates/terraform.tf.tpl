terraform {
  backend "{{ .Backend }}" {
    {{ range .Backend_conf }}
    {{ .Key }} = "{{ .Value }}"
    {{ end }}
  }
}