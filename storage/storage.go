package storage

import (
	project "gitlab.com/jlrosende/api-sandbox/proyect"
)

type Storage interface {
	Save(p project.Proyect) error
	Get(name string) (project.Proyect, error)
	Delete(name string) error
	List() ([]project.Proyect, error)
}
