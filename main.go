package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"text/template"

	project "gitlab.com/jlrosende/api-sandbox/proyect"
)

func main() {

	p := project.NewProyect("", "")
	fmt.Println(p.Name)

	projects_path, ok := os.LookupEnv("PROJECTS_PATH")
	if !ok {
		projects_path = "./temp"
	}

	modules_path, ok := os.LookupEnv("MODULES_PATH")
	if !ok {
		modules_path = "./temp"
	}

	project_name := "test"
	err := os.MkdirAll(filepath.Join(projects_path, project_name), os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}

	files, err := ioutil.ReadDir(modules_path)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		fmt.Println(f.Name())
		fmt.Println(f.IsDir())
	}

	type BackendConf struct {
		Key   string
		Value string
	}

	type TerraformConf struct {
		Backend      string
		Backend_conf []BackendConf
	}

	conf := TerraformConf{
		Backend: "local",
		Backend_conf: []BackendConf{
			{
				Key:   "path",
				Value: filepath.Join(projects_path, project_name, "terraform.tfstate"),
			},
		},
	}

	tmp, err := template.ParseFiles("templates/terraform.tf.tmpl")
	f, err := os.Create(filepath.Join(projects_path, project_name, "terraform.tf"))
	if err != nil {
		panic(err)
	}
	fmt.Println(conf)
	err = tmp.ExecuteTemplate(f, "terraform.tf.tpl", conf)

	if err != nil {
		panic(err)
	}
	f.Close()

	cmd := exec.Command("terraform", "-chdir=./"+projects_path+"/"+project_name, "init")
	stdout, err := cmd.StdoutPipe()
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	for {
		tmpOut := make([]byte, 1024)
		_, err := stdout.Read(tmpOut)
		fmt.Print(string(tmpOut))
		if err != nil {
			break
		}
	}
	for {
		tmpErr := make([]byte, 1024)
		_, err := stderr.Read(tmpErr)
		fmt.Print(string(tmpErr))
		if err != nil {
			break
		}
	}

	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}

	cmd = exec.Command("terraform", "-chdir=./"+projects_path+"/"+project_name, "plan")
	stdout, err = cmd.StdoutPipe()
	stderr, err = cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	for {
		tmpOut := make([]byte, 1024)
		_, err := stdout.Read(tmpOut)
		fmt.Print(string(tmpOut))
		if err != nil {
			break
		}
	}
	for {
		tmpErr := make([]byte, 1024)
		_, err := stderr.Read(tmpErr)
		fmt.Print(string(tmpErr))
		if err != nil {
			break
		}
	}

	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}

	cmd = exec.Command("terraform", "-chdir=./"+projects_path+"/"+project_name, "apply")
	stdout, err = cmd.StdoutPipe()
	stderr, err = cmd.StderrPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	for {
		tmpOut := make([]byte, 1024)
		_, err := stdout.Read(tmpOut)
		fmt.Print(string(tmpOut))
		if err != nil {
			break
		}
	}
	for {
		tmpErr := make([]byte, 1024)
		_, err := stderr.Read(tmpErr)
		fmt.Print(string(tmpErr))
		if err != nil {
			break
		}
	}

	if err := cmd.Wait(); err != nil {
		log.Fatal(err)
	}
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
