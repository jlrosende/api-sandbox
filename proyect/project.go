package project

import (
	"io"
	"os/exec"
)

type Proyect struct {
	Name string
	Path string
}

func NewProyect(name, path string) *Proyect {
	return &Proyect{
		Name: name,
		Path: path,
	}
}

func (p Proyect) Init() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "init")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (p Proyect) Validate() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "validate")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (p Proyect) Plan() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "plan")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (p Proyect) Apply() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "apply")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (p Proyect) Destroy() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "destroy")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}

func (p Proyect) Graph() (io.ReadCloser, io.ReadCloser, error) {
	cmd := exec.Command("terraform", "-chdir=./"+p.Path+"/"+p.Name, "graph")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	return stdout, stderr, nil
}
