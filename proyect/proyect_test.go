package project

import (
	"io"
	"reflect"
	"testing"
)

func TestNewProyect(t *testing.T) {
	type args struct {
		name string
		path string
	}
	tests := []struct {
		name string
		args args
		want *Proyect
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewProyect(tt.args.name, tt.args.path); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProyect() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestProyect_Init(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Init()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Init() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Init() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Init() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestProyect_Validate(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Validate() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Validate() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestProyect_Plan(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Plan()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Plan() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Plan() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Plan() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestProyect_Apply(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Apply()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Apply() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Apply() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Apply() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestProyect_Destroy(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Destroy()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Destroy() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Destroy() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Destroy() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestProyect_Graph(t *testing.T) {
	tests := []struct {
		name    string
		p       Proyect
		want    io.ReadCloser
		want1   io.ReadCloser
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, err := tt.p.Graph()
			if (err != nil) != tt.wantErr {
				t.Errorf("Proyect.Graph() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Proyect.Graph() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("Proyect.Graph() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
